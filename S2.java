import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.io.*;

public class App {
    App() {
        String text = "Swing";
        JFrame frame = new JFrame("Vlad's app");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 400);
        frame.setResizable(false);
        frame.setLayout(null);

        JTextField t1 = new JTextField();
        t1.setBounds(10, 10, 200, 40);

        JTextField t2 = new JTextField();
        t2.setBounds(10, 60, 200, 40);

        JButton b1 = new JButton("Next");
        b1.setBounds(10, 150, 100, 40);
        b1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (t1.getText().equals("")) {
                    t2.setText("");
                    t1.setText(text);
                } else {
                    t1.setText("");
                    t2.setText(text);
                }
            }
        });

        frame.add(t1);
        frame.add(t2);
        frame.add(b1);
        frame.setVisible(true);
    }


    public static void main(String[] args) {
        new App();
    }
}
